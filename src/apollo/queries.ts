
import { gql } from '@apollo/client';

export const GET_CHARACTERS = gql`
  query getCgaracters ($filter:FilterCharacter)  {
    characters(page: 1, filter: $filter) {
      info {
        count
      }
      results {
        id
        name
        image
      }
    }
  }
`;