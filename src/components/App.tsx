import './App.css';
import SearchPannel from './SearchPannel';
import CharacterCard from './CharacterCard';
import { useState } from 'react';
import { useCoDebounce, useCharactersQuery, useParty } from '../hooks';

function App() {

  const [searchFied, setSearchFied] = useState('');
  const debouncedSearchField = useCoDebounce(searchFied, 2, 300);

  const {
    results,
    excludeCard
  } = useCharactersQuery(debouncedSearchField);

  const [party, inviteToParty] = useParty(['Rick', 'Morty']);
  
  function drawResults() {
    return results.map((char) => (
      <CharacterCard
        closeble
        clickable
        key={char.id}
        character={char}
        onClose={excludeCard}
        onClick={inviteToParty}
      />
    ));
  }

  function drawParty() {
    return Object.entries(party).map(([name, char]) => (
      <CharacterCard namePlaseholder={name} key={name} character={char} />
    ));
  }

  return (
    <div className="apparty">
      <header className="apparty__search">
        <SearchPannel value={searchFied} onChange={setSearchFied} />
      </header>
      <section className="apparty__results">
        {drawResults()}
      </section>
      <section className="apparty__party">
        <h1 className="apparty__headeline">
          Party
        </h1>
        {drawParty()}
      </section>
    </div>
  );
}

export default App;
