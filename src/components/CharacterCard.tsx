import noop from 'lodash/fp/noop';
import isNil from 'lodash/fp/isNil';
import isEmpty from 'lodash/fp/isEmpty';
import type { ICharacter } from '../apollo/interfaces';
import './CharacterCard.css';
import React from 'react';

interface ICharacterCardProps {
  character: ICharacter | null,
  namePlaseholder?: string
  closeble?: boolean
  clickable?: boolean,
  onClose?(id: number): void
  onClick?(character: ICharacter): void
}

function CharacterCard({
  character,
  namePlaseholder,
  closeble = false,
  clickable = false,
  onClose = noop,
  onClick = noop
}: ICharacterCardProps) {

  function invokeClick() {
    if (isNil(character)) {
      return;
    }
    onClick(character);
  }

  function closeCard(ev: React.MouseEvent) {
    ev.stopPropagation();
    if (isNil(character)) {
      return;
    }
    onClose(character.id);
  }

  function CloseButton() {
    if (closeble) {
      return <button  className="character-card__close" onClick={closeCard} />;
    }
    return null;
  }

  function Name() {
    if (!isEmpty(namePlaseholder)) {
      return <span className="character-card__name">{ namePlaseholder }</span>;
    }
    return null;
  }

  function Image() {
    if (!isNil(character)) {
      return <img className="character-card__image" src={character.image} alt={character.name} />;
    }
    return null;
  }

  return (
    <div
      role={clickable ? 'button' : 'none'}
      className="character-card"
      title={character?.name}
      onClick={invokeClick}
    >
        <CloseButton />
        <Name />
        <Image />
    </div>
  );
}

export default CharacterCard;