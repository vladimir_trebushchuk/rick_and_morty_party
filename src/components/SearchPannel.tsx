import React from 'react';
import './SearchPannel.css';

interface ISearchPannelProps {
  value: string
  onChange(vaue: string): void
}

function SearchPannel(props: ISearchPannelProps) {

  function onChange(ev: React.ChangeEvent<HTMLInputElement>) {
    console.log(ev.target.value);
    props.onChange(ev.target.value);
  }

  return (
    <div className="search-pannel">
        <input className="search-pannel__input" value={props.value} onChange={onChange} />
    </div>
  );
}

export default SearchPannel;