import { useState, useEffect } from 'react';
import { ICharacter } from './apollo/interfaces';
import { useQuery } from '@apollo/client';
import { GET_CHARACTERS } from './apollo/queries';
import isNill from 'lodash/fp/isNil';

export function useCoDebounce(value: string, length: number, delay: number) {
  const [debouncedValue, setDebouncedValue] = useState(value);

  useEffect(
    () => {
      const timer = setTimeout(() => {
        if (value.length > length) {
          setDebouncedValue(value);
        }
      }, delay);

      return () => {
        clearTimeout(timer);
      };
    },
    [value, length, delay]
  );

  return debouncedValue;
}


type InviteToParty = (character: ICharacter) => void;
interface IParty {
  [name: string]: ICharacter | null
}

export function useParty(guests: string[]): [IParty, InviteToParty] {
  const booking = guests.reduce((list, name) => {
    list[name] = null;
    return list;
  }, {} as IParty);
  const [party, setParty] = useState<IParty>(booking);

  function inviteToParty(character: ICharacter) {
    guests.forEach((name) => {
      if (character.name.includes(name)) {
        setParty((prev) => ({
          ...prev,
          [name]: character
        }));
      }
    });
  }

  return [
    party,
    inviteToParty
  ];
}

export function useCharactersQuery(searchField: string) {
  const [results, setResults] = useState<ICharacter[]>([]);
  const [excludes, setEexcludes] = useState<number[]>([]);

  const { loading, error, data } = useQuery(GET_CHARACTERS, {
    variables: {
      filter: {
        name: searchField
      }
    }
  });

  useEffect(() => {
    setResults((prev) => {
      if (loading) {
        return prev;
      }
      if (isNill(data) || !isNill(error)) {
        return [];
      }
      return data.characters.results.filter(({ id }: ICharacter) => {
        return !excludes.includes(id);
      });
    });
  }, [loading, error, data, excludes]);

  function excludeCard(id: number) {
    setEexcludes((prev) => prev.concat(id));
  }
  return {
    results,
    excludeCard
  };
}