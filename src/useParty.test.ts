import { renderHook, act } from '@testing-library/react-hooks';
import { useParty } from './hooks';

const RICK = 'Rick';
const MORTY = 'Morty';

test('should prepare place for every invited guest', () => {
  const { result } = renderHook(() => {
    const [party, inviteToParty] = useParty([RICK, MORTY]);
    return {
      party,
      inviteToParty
    };
  });
  expect(Object.keys(result.current.party).length).toBe(2);
  expect(result.current.party[RICK]).toBe(null);
  expect(result.current.party[MORTY]).toBe(null);
});

test('should not pass those who weren\'t invited', () => {
  const { result } = renderHook(() => {
    const [party, inviteToParty] = useParty([RICK, MORTY]);
    return {
      party,
      inviteToParty
    };
  });
  act(() => {
    result.current.inviteToParty({
      id: 5,
      name: 'Marry',
      image: 'Image',
    });
  });
  expect(result.current.party[RICK]).toBe(null);
  expect(result.current.party[MORTY]).toBe(null);
});

test('should pass invited guests', () => {
  const { result } = renderHook(() => {
    const [party, inviteToParty] = useParty([RICK, MORTY]);
    return {
      party,
      inviteToParty
    };
  });
  
  act(() => {
    result.current.inviteToParty({
      id: 1,
      name: RICK,
      image: 'Image',
    });
  });
  expect(Object.keys(result.current.party).length).toBe(2);
  expect(result.current.party[RICK]).toHaveProperty('name');
  expect(result.current.party[RICK]?.name).toBe(RICK);
  expect(result.current.party[MORTY]).toBe(null);

  
  act(() => {
    result.current.inviteToParty({
      id: 2,
      name: MORTY,
      image: 'Image',
    });
  });
  expect(Object.keys(result.current.party).length).toBe(2);
  expect(result.current.party[RICK]).toHaveProperty('name');
  expect(result.current.party[RICK]?.name).toBe(RICK);
  expect(result.current.party[MORTY]).toHaveProperty('name');
  expect(result.current.party[MORTY]?.name).toBe(MORTY);
});

